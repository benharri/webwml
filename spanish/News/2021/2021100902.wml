#use wml::debian::translation-check translation="d9e5cd3d7df23feb17458b95c465e062e9cd6e5a"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.11</define-tag>
<define-tag release_date>2021-10-09</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.11</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la undécima actualización de su
distribución «antigua estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «antigua estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction atftp "Corrige desbordamiento de memoria [CVE-2021-41054]">
<correction base-files "Actualizado para la versión 10.11">
<correction btrbk "Corrige problema de ejecución de código arbitrario [CVE-2021-38173]">
<correction clamav "Nueva versión «estable» del proyecto original; corrige violaciones de acceso en clamdscan cuando --fdpass y --multipass se utilizan conjuntamente con ExcludePath">
<correction commons-io "Corrige problema de escalado de directorios [CVE-2021-29425]">
<correction cyrus-imapd "Corrige problema de denegación de servicio [CVE-2021-33582]">
<correction debconf "Comprueba que whiptail o dialog sean realmente utilizables">
<correction debian-installer "Recompilado contra buster-proposed-updates; actualiza la ABI de Linux a la 4.19.0-18">
<correction debian-installer-netboot-images "Recompilado contra buster-proposed-updates">
<correction distcc "Corrige enlaces a compiladores cruzados GCC en update-distcc-symlinks y añade soporte de clang y de CUDA (nvcc)">
<correction distro-info-data "Actualiza datos de varias versiones">
<correction dwarf-fortress "Elimina de los archivos fuente bibliotecas compartidas precompiladas que no son distribuibles">
<correction espeak-ng "Usa espeak con mbrola-fr4 cuando mbrola-fr1 no está instalado">
<correction gcc-mingw-w64 "Corrige tratamiento de gcov">
<correction gthumb "Corrige problema de desbordamiento de memoria dinámica («heap») [CVE-2019-20326]">
<correction hg-git "Corrige fallos en pruebas con versiones recientes de git">
<correction htslib "Corrige autopkgtest en i386">
<correction http-parser "Corrige problema de «contrabando» de peticiones HTTP («HTTP request smuggling») [CVE-2019-15605]">
<correction irssi "Corrige problema de «uso tras liberar» al enviar credenciales SASL al servidor [CVE-2019-13045]">
<correction java-atk-wrapper "Utiliza dbus también para detectar si la accesibilidad está habilitada">
<correction krb5 "Corrige caída del KDC por desreferencia nula en peticiones FAST que no incluyen el campo server [CVE-2021-37750]; corrige fuga de memoria en krb5_gss_inquire_cred">
<correction libdatetime-timezone-perl "Nueva versión «estable» del proyecto original; actualiza reglas de DST para Samoa y Jordania; confirmación de ausencia de segundo intercalar el 31 de diciembre de 2021">
<correction libpam-tacplus "Evita la escritura en el log del sistema y en texto claro de secretos compartidos [CVE-2020-13881]">
<correction linux "<q>proc: hace seguimiento de la mm_struct de quien ha abierto /proc/$pid/attr/</q>, corrigiendo problemas con lxc-attach; nueva versión «estable» del proyecto original; incrementa versión de la ABI a la 18; [rt] actualiza a la 4.19.207-rt88; usb: hso: corrige código de tratamiento de errores en hso_create_net_device [CVE-2021-37159]">
<correction linux-latest "Actualizado a la ABI 4.19.0-18 del núcleo">
<correction linux-signed-amd64 "<q>proc: hace seguimiento de la mm_struct de quien ha abierto /proc/$pid/attr/</q>, corrigiendo problemas con lxc-attach; nueva versión «estable» del proyecto original; incrementa versión de la ABI a la 18; [rt] actualiza a la 4.19.207-rt88; usb: hso: corrige código de tratamiento de errores en hso_create_net_device [CVE-2021-37159]">
<correction linux-signed-arm64 "<q>proc: hace seguimiento de la mm_struct de quien ha abierto /proc/$pid/attr/</q>, corrigiendo problemas con lxc-attach; nueva versión «estable» del proyecto original; incrementa versión de la ABI a la 18; [rt] actualiza a la 4.19.207-rt88; usb: hso: corrige código de tratamiento de errores en hso_create_net_device [CVE-2021-37159]">
<correction linux-signed-i386 "<q>proc: hace seguimiento de la mm_struct de quien ha abierto /proc/$pid/attr/</q>, corrigiendo problemas con lxc-attach; nueva versión «estable» del proyecto original; incrementa versión de la ABI a la 18; [rt] actualiza a la 4.19.207-rt88; usb: hso: corrige código de tratamiento de errores en hso_create_net_device [CVE-2021-37159]">
<correction mariadb-10.3 "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2021-2389 CVE-2021-2372]; corrige ruta al ejecutable Perl en scripts">
<correction modsecurity-crs "Corrige problema de elusión de los cuerpos de solicitudes [CVE-2021-35368]">
<correction node-ansi-regex "Corrige problema de denegación de servicio relacionado con expresiones regulares [CVE-2021-3807]">
<correction node-axios "Corrige problema de denegación de servicio relacionado con expresiones regulares [CVE-2021-3749]">
<correction node-jszip "Usa un objeto prototipo nulo para this.files [CVE-2021-23413]">
<correction node-tar "Borra rutas que no corresponden a directorios de la caché de directorios [CVE-2021-32803]; elimina rutas absolutas de forma más completa [CVE-2021-32804]">
<correction nvidia-cuda-toolkit "Corrige el valor de NVVMIR_LIBRARY_DIR en ppc64el">
<correction nvidia-graphics-drivers "Nueva versión «estable» del proyecto original; corrige problemas de denegación de servicio [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095]; nvidia-driver-libs: Añade Recomienda: libnvidia-encode1">
<correction nvidia-graphics-drivers-legacy-390xx "Nueva versión «estable» del proyecto original; corrige problemas de denegación de servicio [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095]; nvidia-legacy-390xx-driver-libs: Añade Recomienda: libnvidia-legacy-390xx-encode1">
<correction postgresql-11 "Nueva versión «estable» del proyecto original; corrige error de planificación de la aplicación repetida de un paso de proyección [CVE-2021-3677]; rechaza renegociación de SSL de forma más completa">
<correction proftpd-dfsg "Corrige <q>mod_radius leaks memory contents to radius server</q> («mod_radius filtra contenido de la memoria a servidor radius»), <q>cannot disable client-initiated renegotiation for FTPS</q> («no puedo inhabilitar la renegociación de FTPS iniciada por el cliente»), navegación a directorios vía enlace simbólico, caída de mod_sftp al usar autenticación mediante clave pública con claves DSA">
<correction psmisc "Corrige regresión en killall: proceso no encontrado con nombres de más de 15 caracteres">
<correction python-uflash "Actualiza URL del firmware">
<correction request-tracker4 "Corrige problema de ataque de canal lateral de sincronización en el acceso al sistema («login») [CVE-2021-38562]">
<correction ring "Corrige problema de denegación de servicio en la copia de pjproject embebida [CVE-2021-21375]">
<correction sabnzbdplus "Evita escape del directorio en función renamer [CVE-2021-29488]">
<correction shim "Añade parche para arm64 para modificar la disposición de las secciones y para evitar problemas de caídas; en modo inseguro, no aborta si no podemos definir la variable MokListXRT; si falla la instalación de grub, emite un aviso en lugar de abortar">
<correction shim-helpers-amd64-signed "Añade parche para arm64 para modificar la disposición de las secciones y para evitar problemas de caídas; en modo inseguro, no aborta si no podemos definir la variable MokListXRT; si falla la instalación de grub, emite un aviso en lugar de abortar">
<correction shim-helpers-arm64-signed "Añade parche para arm64 para modificar la disposición de las secciones y para evitar problemas de caídas; en modo inseguro, no aborta si no podemos definir la variable MokListXRT; si falla la instalación de grub, emite un aviso en lugar de abortar">
<correction shim-helpers-i386-signed "Añade parche para arm64 para modificar la disposición de las secciones y para evitar problemas de caídas; en modo inseguro, no aborta si no podemos definir la variable MokListXRT; si falla la instalación de grub, emite un aviso en lugar de abortar">
<correction shim-signed "Solución provisional de problemas que impiden el arranque en arm64 incluyendo una versión funcional de shim no firmado en esa plataforma; vuelve atrás arm64 para usar una compilación no firmada actual; añade parche para arm64 para modificar la disposición de las secciones y para evitar problemas de caídas; en modo inseguro, no aborta si no podemos definir la variable MokListXRT; si falla la instalación de grub, emite un aviso en lugar de abortar">
<correction shiro "Corrige problemas de elusión de autenticación [CVE-2020-1957 CVE-2020-11989 CVE-2020-13933 CVE-2020-17510]; actualiza parche de compatibilidad con Spring Framework; soporte para Guice 4">
<correction tzdata "Actualiza reglas de DST para Samoa y Jordania; confirmación de ausencia de segundo intercalar el 31 de diciembre de 2021">
<correction ublock-origin "Nueva versión «estable» del proyecto original; corrige problema de denegación de servicio [CVE-2021-36773]">
<correction ulfius "Se asegura de que la memoria está inicializada antes de usarla [CVE-2021-40540]">
<correction xmlgraphics-commons "Corrige problema de falsificación de solicitudes en el lado servidor («Server-Side Request Forgery») [CVE-2020-11988]">
<correction yubikey-manager "Añade dependencia de yubikey-manager con python3-pkg-resources, que faltaba">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «antigua estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2021 4842 thunderbird>
<dsa 2021 4866 thunderbird>
<dsa 2021 4876 thunderbird>
<dsa 2021 4897 thunderbird>
<dsa 2021 4927 thunderbird>
<dsa 2021 4931 xen>
<dsa 2021 4932 tor>
<dsa 2021 4933 nettle>
<dsa 2021 4934 intel-microcode>
<dsa 2021 4935 php7.3>
<dsa 2021 4936 libuv1>
<dsa 2021 4937 apache2>
<dsa 2021 4938 linuxptp>
<dsa 2021 4939 firefox-esr>
<dsa 2021 4940 thunderbird>
<dsa 2021 4941 linux-signed-amd64>
<dsa 2021 4941 linux-signed-arm64>
<dsa 2021 4941 linux-signed-i386>
<dsa 2021 4941 linux>
<dsa 2021 4942 systemd>
<dsa 2021 4943 lemonldap-ng>
<dsa 2021 4944 krb5>
<dsa 2021 4945 webkit2gtk>
<dsa 2021 4946 openjdk-11-jre-dcevm>
<dsa 2021 4946 openjdk-11>
<dsa 2021 4947 libsndfile>
<dsa 2021 4948 aspell>
<dsa 2021 4949 jetty9>
<dsa 2021 4950 ansible>
<dsa 2021 4951 bluez>
<dsa 2021 4952 tomcat9>
<dsa 2021 4953 lynx>
<dsa 2021 4954 c-ares>
<dsa 2021 4955 libspf2>
<dsa 2021 4956 firefox-esr>
<dsa 2021 4957 trafficserver>
<dsa 2021 4958 exiv2>
<dsa 2021 4959 thunderbird>
<dsa 2021 4961 tor>
<dsa 2021 4962 ledgersmb>
<dsa 2021 4963 openssl>
<dsa 2021 4964 grilo>
<dsa 2021 4967 squashfs-tools>
<dsa 2021 4969 firefox-esr>
<dsa 2021 4970 postorius>
<dsa 2021 4971 ntfs-3g>
<dsa 2021 4973 thunderbird>
<dsa 2021 4974 nextcloud-desktop>
<dsa 2021 4975 webkit2gtk>
<dsa 2021 4979 mediawiki>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction birdtray "Incompatible con versiones más recientes de Thunderbird">
<correction libprotocol-acme-perl "Solo soporta ACME versión 1, que es obsoleto">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «antigua estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «antigua estable» actual:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Actualizaciones propuestas a la distribución «antigua estable»:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Información sobre la distribución «antigua estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


