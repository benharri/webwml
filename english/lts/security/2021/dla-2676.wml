<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues were discovered in Django, the Python-based web development
framework:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33203">CVE-2021-33203</a>

    <p>Potential directory traversal via admindocs</p>

    <p>Staff members could use the admindocs TemplateDetailView view to check
    the existence of arbitrary files. Additionally, if (and only if) the
    default admindocs templates have been customized by the developers to also
    expose the file contents, then not only the existence but also the file
    contents would have been exposed.</p>

    <p>As a mitigation, path sanitation is now applied and only files within
    the template root directories can be loaded.</p>

    <p>This issue has low severity, according to the Django security policy.
    Thanks to Rasmus Lerchedahl Petersen and Rasmus Wriedt Larsen from the
    CodeQL Python team for the report.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33571">CVE-2021-33571</a>

    <p>Possible indeterminate SSRF, RFI, and LFI attacks since validators
    accepted leading zeros in IPv4 addresses</p>

    <p>URLValidator, validate_ipv4_address(), and validate_ipv46_address()
    didn't prohibit leading zeros in octal literals. If you used such values
    you could suffer from indeterminate SSRF, RFI, and LFI attacks.</p>

    <p>validate_ipv4_address() and validate_ipv46_address() validators were not
    affected on Python 3.9.5+.</p>

    <p>This issue has medium severity, according to the Django security
    policy.</p>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:1.10.7-2+deb9u14.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2676.data"
# $Id: $
