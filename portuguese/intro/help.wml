#use wml::debian::template title="Contribuir: como você pode ajudar o Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="d20fb9052ff8188ede3f9401187cb5ccfd2cdd43"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li> <a href="#coding">Codifique e mantenha pacotes</a></li>
    <li> <a href="#testing">Teste e elimine bugs</a></li>
    <li> <a href="#documenting">Escreva documentação e marque pacotes</a></li>
    <li> <a href="#translating">Traduza e localize</a></li>
    <li> <a href="#usersupport">Ajude outros(as) usuários(as)</a></li>
    <li> <a href="#events">Organize eventos</a></li>
    <li> <a href="#donations">Doe dinheiro, hardware ou largura de banda</a></li>
    <li> <a href="#usedebian">Use o Debian e fale sobre ele</a></li>
    <li> <a href="#organizations">Como sua organização pode ajudar o Debian</a></li>
  </ul>
</div>

<p>O Debian não é apenas um sistema operacional, é uma comunidade. Muitas
pessoas com muitas habilidades diferentes contribuem para o projeto: nosso
software, a arte, o wiki e outras documentações são resultados de um esforço
conjunto de um grande grupo de pessoas. Nem todo mundo é desenvolvedor(a), e
você certamente não precisa saber programar se quiser participar. Existem
muitas maneiras diferentes de ajudar a tornar o Debian ainda melhor. Se você
gostaria de participar, aqui estão algumas sugestões para usuários(as)
experientes e inexperientes. </p>

<h2><a id="coding">Codifique e mantenha pacotes</a></h2>

<aside class="light">
  <span class="fa fa-code-branch fa-5x"></span>
</aside>

<p>Talvez você queira escrever um novo aplicativo do zero, talvez você queira
implementar um novo recurso em um programa existente. Se você é um(a)
desenvolvedor(a) e deseja contribuir com o Debian, também pode nos ajudar
a preparar o software no Debian para uma instalação fácil, nós o chamamos de
"empacotamento". Dê uma olhada nesta lista para obter algumas ideias sobre como
começar:</p>

<ul>
  <li>Empacote aplicativos, por exemplo aqueles que você tem experiência ou
considera valiosos para o Debian. Para mais informações sobre como se tornar
um(a) mantenedor(a) de pacote, visite o
<a href="$(HOME)/devel/">canto dos(as) desenvolvedores(as) Debian</a>.</li>
  <li>Ajude a manter os aplicativos existentes, por exemplo, contribuindo com
correções (patches) ou informações adicionais no
<a href="https://bugs.debian.org/">sistema de rastreamento de bugs</a>.
Alternativamente, você pode ingressar em uma equipe de manutenção de grupo ou
ingressar em um projeto de software no
<a href="https://salsa.debian.org/">Salsa</a> (nossa própria instância
GitLab).</li>
  <li>Ajude-nos <a href="https://security-tracker.debian.org/tracker/data/report">rastreando</a>,
<a href="$(HOME)/security/audit/">encontrando</a> e
<a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">consertando</a>
<a href="$(HOME)/security/">problemas de segurança</a> no Debian.</li>
  <li>Você também pode ajudar com o fortalecimento de <a href="https://wiki.debian.org/Hardening">pacotes</a>,
<a href="https://wiki.debian.org/Hardening/RepoAndImages">repositórios e imagens</a>
e <a href="https://wiki.debian.org/Hardening/Goals">outros componentes</a>.</li>
  <li>Interessado(a) em <a href="$(HOME)/ports/">portar</a> o Debian para alguma
arquitetura com a qual você está familiarizado(a)? Você pode iniciar um novo
porte ou contribuir para um existente.</li>
  <li>Ajude-nos a melhorar os
<a href="https://wiki.debian.org/Services">serviços</a> relacionados ao Debian
ou crie e mantenha novos serviços,
<a href="https://wiki.debian.org/Services#wishlist">sugeridos ou solicitados</a>
pela comunidade.</li>
</ul>

<h2><a id="testing">Teste e elimine bugs</a></h2>

<aside class="light">
  <span class="fa fa-bug fa-5x"></span>
</aside>

<p>Como qualquer outro projeto de software, o Debian precisa de usuários(as)
que testem o sistema operacional e seus aplicativos. Uma forma de contribuir é
instalar a versão mais recente e relatar aos(as) desenvolvedores(as) se algo não
funcionar como deveria. Também precisamos de pessoas para testar nossa mídia de
instalação, o boot seguro e o carregador de boot U-Boot em hardwares diferentes.
</p>

<ul>
  <li>Você pode usar nosso <a href="https://bugs.debian.org/">sistema de rastreamento de bugs</a>
para relatar qualquer problema que encontrar no Debian. Antes disso,
certifique-se de que o bug já não tenha sido relatado.</li>
  <li>Visite o rastreador de bugs e tente navegar pelos bugs associados aos
pacotes que você usa. Veja se você pode fornecer mais informações e reproduzir
os problemas descritos.</li>
  <li>Teste o
<a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">instalador e as imagens ISO live</a>
do Debian, o
<a href="https://wiki.debian.org/SecureBoot/Testing">suporte ao boot seguro</a>, as
<a href="https://wiki.debian.org/LTS/TestSuites">atualizações LTS</a> e o
carregador de boot
<a href =" https://wiki.debian.org/U-boot/Status">U-Boot</a>.</li>
</ul>

<h2><a id="documenting">Escreva documentação e marque pacotes</a></h2>

<aside class="light">
  <span class="fa fa-file-alt fa-5x"></span>
</aside>

<p>Se você tiver problemas no Debian e não conseguir escrever código para
resolvê-los, talvez fazer anotações e escrever sua solução seja uma opção para
você. Dessa forma, pode ajudar outros(as) usuários(as) que têm
problemas semelhantes. Toda a documentação do Debian é escrita por membros(as)
da comunidade e existem várias maneiras de você ajudar.</p>

<ul>
  <li>Ingresse no <a href="$(HOME)/doc/ddp">projeto de documentação do Debian</a>
para ajudar com a documentação oficial do Debian.</li>
  <li>Contribua para a <a href="https://wiki.debian.org/">Wiki do Debian</a>.</li>
  <li>Marque e categorize pacotes no site web <a href="https://debtags.debian.org/">debtags</a>,
para que nossos(as) usuários(as) possam encontrar softwares mais facilmente.</li>
</ul>

<h2><a id="translating">Traduza e localize</a></h2>

<aside class="light">
  <span class="fa fa-language fa-5x"></span>
</aside>

<p>
Seu idioma nativo não é o inglês, mas você tem bons conhecimentos de inglês o
suficiente para entender e traduzir software ou informações relacionadas ao
Debian como páginas web, documentação, etc? Por que não se juntar a uma equipe
de tradução e converter aplicativos Debian para sua língua materna? Também
estamos procurando pessoas para verificar as traduções existentes e enviar
relatórios de erros, se necessário.
</p>

# Tradutores, crie um link diretamente para as páginas do seu grupo
<ul>
  <li>Tudo relacionado à internacionalização do Debian é discutido na
  <a href="https://lists.debian.org/debian-i18n/">lista de discussão i18n</a>.</li>
  <li>Você é um(a) falante nativo(a) de um idioma que ainda não é suportado no
Debian? Entre em contato por meio da página
<a href="$(HOME)/international/">Debian internacional</a>.</li>
  <li>O projeto de tradução para português é coordenado na
<a href="https://lists.debian.org/debian-l10n-portuguese/">lista de discussão l10n-portuguese</a>.</li>
  <li>Você encontrará mais informações do projeto de tradução do Debian para o
português nas
<a href="https://www.debian.org/international/Portuguese">páginas do projeto</a>.</li>
</ul>

<h2><a id="usersupport">Ajude outros(as) usuários(as)</a></h2>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>

<p>Você também pode contribuir com o projeto ajudando outros(as) usuários(as)
Debian. O projeto usa vários canais de suporte, por exemplo, listas de discussão
em diferentes idiomas e canais de IRC. Para obter mais informações,
visite nossas <a href="$(HOME)/support">páginas de suporte</a>.</p>

# Tradutores, faça um link diretamente para o lista de discussão do seu idioma e
# providencie um link para o canal de IRC na língua do usuário
<ul>
   <li>O projeto Debian usa muitas
<a href="https://lists.debian.org/">listas de discussão</a> diferentes; algumas
são para desenvolvedores(as) e algumas são para usuários(as). Usuários(as)
experientes podem ajudar outras pessoas por meio das
<a href="$(HOME)/support#mail_lists"> listas de discussão de usuários(as)</a>.</li>
   <li>Pessoas de todo o mundo conversam em tempo real no IRC (Internet Relay
Chat). Visite o canal <tt>#debian</tt> no
<a href="https://www.oftc.net/">OFTC</a> para conversar com outros(as)
usuários(as) Debian.</li>
   <li>Para ajudar usuários(as) em português, visite a
<a href="https://lists.debian.org/debian-user-portuguese">lista de discussão debian-user-portuguese</a>
e o canal <tt>#debian-br</tt> no <a href="https://www.oftc.net/">OFTC</a>.</li>
</ul>

<h2><a id="events">Organize eventos</a></h2>

<aside class="light">
  <span class="fas fa-calendar-check fa-5x"></span>
</aside>

<p>Além da conferência anual Debian (DebConf), existem várias reuniões e
encontros menores e em diferentes países todos os anos. Participar ou ajudar a
organizar um <a href="$(HOME)/events/">evento</a> é uma grande oportunidade de
conhecer outros(as) usuários(as) e desenvolvedores(as) Debian.</p>

<ul>
  <li>Ajude durante a <a href="https://debconf.org/">conferência anual Debian</a>,
por exemplo gravando <a href="https://video.debconf.org/">vídeos</a> das
palestras e apresentações, recebendo os(as) participantes e ajudando os
palestrantes, organizando eventos especiais durante a DebConf (como a festa dos
queijos e do vinhos), ajudando na montagem e desmontagem, etc.</li>
  <li>Além disso, existem vários eventos chamados de
<a href="https://wiki.debian.org/MiniDebConf">MiniDebConf</a>, reuniões locais
organizadas por membros(as) do projeto Debian.</li>
  <li>Você também pode criar ou ingressar em um
<a href="https://wiki.debian.org/LocalGroups">grupo Debian local</a> com
reuniões regulares ou outras atividades.</li>
  <li>Além disso, verifique outros eventos como
<a href="https://wiki.debian.org/DebianDay">festas do dia do Debian (Debian day)</a>,
<a href="https://wiki.debian.org/ReleaseParty">festas de lançamento (release parties)</a>,
<a href="https://wiki.debian.org/BSP">festas de caça aos bugs (bug squashing parties)</a>,
<a href="https://wiki.debian.org/Sprints">sprints de desenvolvimento</a> ou
<a href="https://wiki.debian.org/DebianEvents">outros eventos</a> pelo mundo.</li>
  <li>Você pode ver a <a href="https://wiki.debian.org/Brasil/Eventos/">lista de eventos no Brasil</a>
e participar da
<a href="https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos">lista de discussão debian-br-eventos</a>
para saber mais sobre esses eventos ou ajudar a organizá-los no nosso país.</li>
</ul>

<h2><a id="donations">Doe dinheiro, hardware ou largura de banda</a></h2>

<aside class="light">
  <span class="fas fa-gift fa-5x"></span>
</aside>

<p>Todas as doações para o projeto Debian são gerenciadas por nosso(a) líder
do projeto Debian (DPL - Debian Project Leader). Com o seu apoio, podemos
comprar hardware, domínios, certificados criptográficos, etc. Também usamos
fundos para patrocinar os eventos DebConf e MiniDebConf, sprints de
desenvolvimento, presença em outros eventos e outras coisas. </p>

<ul>
  <li>Você pode <a href="$(HOME)/donations">doar</a> dinheiro, equipamentos e
serviços para o projeto Debian.</li>
  <li>Estamos constantemente procurando por <a href="$(HOME)/mirror/">espelhos</a>
no mundo todo.</li>
  <li>Para nossos portes Debian, contamos com nossa
<a href="$(HOME)/devel/buildd/">rede autobuilder</a>.</li>
</ul>

<h2><a id="usedebian">Use o Debian e fale sobre ele</a></h2>

<aside class="light">
  <span class="fas fa-bullhorn fa-5x"></span>
</aside>

<p>Divulgue e fale para outras pessoas sobre o Debian e a comunidade Debian.
Recomende o sistema operacional a outros(as) usuários(as) e mostre como
instalá-lo. Simplesmente use-o e divirta-se -- essa é provavelmente a maneira
mais fácil de retribuir ao projeto Debian.</p>

<ul>
   <li>Ajude a promover o Debian dando uma palestra e demonstrando para
outros(as) usuários(as).</li>
   <li>Contribua com nosso
<a href="https://www.debian.org/devel/website/">site web</a> e nos ajude a
melhorar a face pública do Debian.</li>
   <li>Faça <a href="https://wiki.debian.org/ScreenShots">capturas de tela</a>
e faça o <a href="https://screenshots.debian.net/upload">upload</a> delas para
<a href="https://screenshots.debian.net/"> screenshots.debian.net</a> para que
nossos(as) usuários(as) possam ver a aparência do software no Debian antes de
usá-lo.</li>
   <li>Você pode habilitar
<a href="https://packages.debian.org/popularity-contest">envios para o concurso de popularidade</a>
para que possamos saber quais pacotes são populares e mais úteis para
todos(as).</li>
</ul>

<h2><a id="organizations">Como sua organização pode ajudar o Debian</a></h2>

<aside class="light">
  <span class="fa fa-check-circle fa-5x"></span>
</aside>

<p>
Quer você trabalhe em uma organização educacional, comercial, sem fins
lucrativos ou governamental, existem muitas maneiras de nos apoiar com seus
recursos.
</p>

<ul>
  <li>Por exemplo, sua organização pode simplesmente
<a href="$(HOME)/donations">doar</a> dinheiro ou hardware.</li>
  <li>Talvez você queira
<a href="https://www.debconf.org/sponsors/">patrocinar</a> nossas conferências.</li>
  <li>Sua organização pode fornecer
<a href="https://wiki.debian.org/MemberBenefits">produtos ou serviços para contribuidores(as) Debian</a>.</li>
  <li>Também estamos procurando por <a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">hospedagem gratuita</a>.</li>
  <li>Claro, configurando espelhos para nosso
<a href="https://www.debian.org/mirror/ftpmirror">software</a>, para
<a href="https://www.debian.org/CD/mirroring/">mídias de instalação</a> ou para
<a href="https://wiki.debconf.org/wiki/Videoteam/Archive">vídeos das conferências</a>
também é muito apreciado.</li>
  <li>Talvez você também considere vender
<a href="https://www.debian.org/events/merchandise">merchandise</a> do Debian,
<a href="https://www.debian.org/CD/vendors/">mídias de instalação</a> ou
<a href="https://www.debian.org/distrib/pre-installed">sistemas pré-instalados</a>.</li>
  <li>Se sua organização oferece
<a href="https://www.debian.org/consultants/">consultoria</a> ou
<a href="https://wiki.debian.org/DebianHosting">hospedagem</a>, nos informe.</li>
</ul>

<p>
Também estamos interessados(as) em formar
<a href="https://www.debian.org/partners/">parcerias</a>. Se você pode promover
o Debian <a href="https://www.debian.org/users/">fornecendo um depoimento</a>,
rodando nos servidores ou desktops de sua organização, ou mesmo encorajando sua
equipe a participar do nosso projeto, isso é fantástico. Talvez você também
considere ensinar sobre o sistema operacional Debian e a comunidade,
direcionando sua equipe para contribuir durante o horário de trabalho ou
enviando-os para um de nossos <a href="$(HOME)/events/">eventos</a>.</p>

