#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14404">CVE-2018-14404</a>
<p>Correction d’un déréférencement de pointeur NULL pouvant aboutir à un
 plantage et donc à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14567">CVE-2018-14567</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-9251">CVE-2018-9251</a>
<p>Adoption du traitement d’erreur LZMA évitant une boucle infinie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18258">CVE-2017-18258</a>

<p>Limitation de la mémoire disponible à 100 MB pour éviter une consommation
excessive de mémoire par des fichiers malveillants.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.9.1+dfsg1-5+deb8u7.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libxml2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1524.data"
# $Id: $
