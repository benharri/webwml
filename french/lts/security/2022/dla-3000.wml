#use wml::debian::translation-check translation="6ab83bc40a33e4482bd19c8a9e77e9a7cbc55098" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Waitress est un serveur WSGI en Python, un serveur d'applications pour
les applications web Python.</p>

<p>Ces mises à jour de sécurité corrigent des bogues de dissimulation de
requête, quand Waitress est combiné avec un autre mandataire HTTP qui
interprète les requêtes différemment. Cela peut conduire à une
possibilité de dissimulation ou de fractionnement de requête HTTP au moyen
desquelles Waitress peut voir deux requêtes tandis que le serveur frontal
ne voit qu'un seul message HTTP. Cela peut avoir pour conséquence un
empoisonnement de cache ou une divulgation imprévue d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16785">CVE-2019-16785</a>

<p>Waitress reconnaît uniquement CRLF comme une fin de ligne, et nom un
saut de ligne complet (LF). Avant cette modification, Waitress pouvait voir
deux requêtes là où le mandataire frontal n'en voyait qu'une.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16786">CVE-2019-16786</a>

<p>Waitress pourrait analyser l'en-tête Transfer-Encoding en ne recherchant
qu'une seule valeur de chaîne, si cette valeur n'a pas été <q>chunked</q>,
il pourrait passer à travers et utiliser l'en-tête Content-Length à la
place. Cela pourrait permettre à Waitress de traiter une requête unique
comme des requêtes multiples dans le cas d'un pipeline HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16789">CVE-2019-16789</a>

<p>De requêtes contrefaites pour l'occasion contenant des caractères
d'espace particuliers dans l'en-tête Transfer-Encoding pourraient être
analysées par Waitress comme étant une requête « chunked », mais un serveur
frontal pourrait utiliser l'en-tête Content-Length à la place dans la
mesure où l'en-tête Transfer-Encoding est considéré comme non valable parce
qu'il contient des caractères non valables.Si un serveur frontal établi un
pipeline HTTP vers un serveur dorsal Waitress, cela pourrait conduire au
fractionnement de la requête HTTP qui pourrait mener à un potentiel
empoisonnement de cache ou à une divulgation imprévue d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16792">CVE-2019-16792</a>

<p>Si deux en-têtes Content-Length sont envoyés dans une requête unique,
Waitress traiterait la requête comme n'ayant pas de corps, tout en traitant
le corps de la requête comme une nouvelle requête dans un tunnel HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24761">CVE-2022-24761</a>

<p>Il y a deux classes de vulnérabilité qui peuvent conduire à une
dissimulation de requête qui ont été traitées par cette alerte :</p>

<ol>
<li>L'utilisation d'int() de Python pour analyser des chaînes dans des
entiers faisant que +10 est analysé comme 10, ou 0x01 comme 1, où la norme
spécifie que la chaîne ne devrait contenir que des chiffres ou des chiffres
hexadécimaux.</li>
<li>Waitress ne gère pas les extensions de bloc, néanmoins il les rejette
sans confirmer qu'elles ne contiennent pas de caractères interdits.</li>
</ol>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.0.1-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets waitress.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de waitress, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/waitress">\
https://security-tracker.debian.org/tracker/waitress</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3000.data"
# $Id: $
