#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2816">CVE-2017-2816</a>

<p>Une vulnérabilité exploitable de dépassement de tampon existe dans la
fonctionnalité d’analyse de balises de LibOFX 0.9.11. Un fichier OFX
spécialement contrefait peut causer une écriture hors limites aboutissant à un
dépassement de tampon dans la pile. Un attaquant peut construire un fichier OFX
malveillant pour déclencher cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14731">CVE-2017-14731</a>

<p>ofx_proc_file dans ofx_preproc.cpp permet à des attaquants distants de
provoquer un déni de service (lecture hors limites de tampon basé sur le tas et
plantage d'application) à l'aide d'un fichier contrefait</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:0.9.4-2.1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libofx.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1192.data"
# $Id: $
