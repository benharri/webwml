#use wml::debian::template title="다른 언어에서 웹 사이트" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="80696988195221adfe32e0c037530145acd35b48" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#intro">내용 검색</a></li>
    <li><a href="#howtoset">웹 브라우저 언어 설정을 어떻게 하는가</a></li>
    <li><a href="#override">어떻게 설정을 무시하나</a></li>
    <li><a href="#fix">문제 해결</a></li>
  </ul>
</div>

<h2><a id="intro">내용 검색</a></h2>

<p>
<a href="../devel/website/translating">번역</a>팀이 데비안 웹사이트에서 작업하여 점점 더 많은 다른 언어로 번역하고 있습니다. 
그러나 웹 브라우저에서 언어 전환은 어떻게 동작하나요?  
<a href="$(HOME)/devel/website/content_negotiation">내용 협상</a>이라는 표준을 통해 
사용자는 웹 내용에 대해 기본 언어를 설정할 수 있습니다. 
그들이 보는 버전을 웹 브라우저와 웹 서버 간에 협상합니다.
브라우저는 기본 설정을 서버로 보내고 서버는 전달할 버전을 결정합니다.
(사용자의 기본 설정 및 사용 가능한 버전에 따라).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="http://www.w3.org/International/questions/qa-lang-priorities">W3C에서 더 읽기</a></button></p>

<p>
모든 사람이 내용 협상에 대해 아는 것은 아니므로 모든 데비안 페이지 하단에 있는 링크는 사용 가능한 다른 버전으로 연결됩니다. 
이 목록에서 다른 언어를 선택하면 현재 페이지에만 영향을 미칩니다. 
웹 브라우저의 기본 언어는 안 바뀝니다. 다른 링크를 따라 다른 페이지로 이동하면 기본 언어로 다시 나타납니다.
</p>

<p>
기본 언어를 바꾸는 두 옵션:
</p>

<ul>
  <li><a href="#howtoset">웹 브라우저 설정</a></li>
  <li><a href="#override">브라우저 언어 설정 무시</a></li>
</ul>

<p>
이 웹 브라우저에 대한 구성 명령으로 건너 뛰기:</p>

<toc-display />

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>
데비안 웹사이트의 원래 언어는 영어입니다. 따라서 언어 목록의 맨 아래에 영어(<code>en</code>)를 추가하는 것이 좋습니다. 
페이지가 원하는 언어로 아직 번역되지 않은 경우 백업으로 동작합니다.</p>
</aside>

<h2><a id="howtoset">어떻게 웹 브라우저 언어 설정을 하는가</a></h2>

<p>
서로 다른 웹 브라우저에서 언어 설정을 구성하는 방법을 설명하기 전에 몇 가지 일반적인 비고를 설명하겠습니다. 
첫째, 기본 언어 목록에 사용하는 모든 언어를 포함하는 것이 좋습니다. 
예를 들어, 한국어를 모국어로 사용하는 경우, <code>ko</code>를 첫 번째 언어로 선택한 다음, 
언어 코드 <code>en</code>으로 영어를 선택할 수 있습니다.
</p>

<p>
둘째, 일부 브라우저에서는 메뉴에서 선택하는 대신 언어 코드를 입력할 수 있습니다. 
이 경우 <code>fr, en</code>과 같은 목록을 만드는 것은 기본 설정을 정의하지 않습니다. 
대신, 동일한 순위가 매겨진 옵션을 정의하고, 웹 서버는 순서를 무시하고 언어 중 하나를 선택하도록 결정할 수 있습니다. 
실제 기본 설정을 지정하려면 소위 품질 값, 즉 0과 1 사이의 부동 소수점 값으로 작업해야 합니다. 
값이 클수록 우선순위가 높아집니다. 
프랑스어와 영어로 된 예시로 돌아가면 위의 예제를 다음과 같이 수정할 수 있습니다:
</p>

<pre>
fr; q=1.0, en; q=0.5
</pre>

<h3>언어 코드 주의</h3>

<p>
기본 언어 <code>en-GB, fr</code>로 문서에 대한 요청을 수신하는 웹 서버는 
프랑스어보다 먼저 영어 버전을 <strong>언제나 제공하지는 않습니다</strong>. 
언어 확장자가 <code>en-gb</code>인 페이지가 있는 경우에만 그렇게 합니다. 
다른 방식으로 동작합니다. 
기본 언어 목록에 <code>en</code>만 포함되어 있으면 서버가 <code>en-us</code> 페이지를 반환할 수 있습니다.
</p>

<p>
따라서 특별한 이유가 없는 한 <code>en-GB</code> 또는 <code>en-US</code> 처럼 두 글자 국가 코드를 추가하지 않는 것이 좋습니다. 
하나를 추가하는 경우 확장자가 없는 언어 코드도 포함해야 합니다: <code>en-GB, en, fr</code>
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://httpd.apache.org/docs/current/content-negotiation.html">내용협상 더 보기</a></button></p>

<h3>다른 웹 브라우저를 위한 명령</h3>

<p>
인기 있는 웹 브라우저 목록과 설정에서 웹 콘텐츠에 대한 기본 언어를 변경하는 방법에 대한 몇 가지 명령을 모았습니다.
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="chromium">Chrome/Chromium</toc-add-entry></strong><br>
  At the top right, open the menu and click <em>Settings</em> -&gt; <em>Advanced</em> -&gt; <em>Languages</em>. Open the <em>Language</em> menu to see a list of languages. Click on the three dots next to an entry to change the order. You can also add new languages if necessary.</li>
        <li><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong><br>
  Setting the default language via <em>Setup</em> -&gt; <em>Language</em> will also change the requested language from web sites. You can change this behavior and fine-tune the <em>Accept-Language header</em> at <em>Setup</em> -&gt; <em>Options manager</em> -&gt; <em>Protocols</em> -&gt; <em>HTTP</em></li>
        <li><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong><br>
  Open <em>Preferences</em> from the main menu and switch to the <em>Language</em> tab. Here you can add, remove and arrange languages.</li>
        <li><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong><br>
  In the menu bar at the top open <em>Preferences</em>. Scroll down to <em>Language and appearance</em> -&gt; <em>Language</em> in the <em>General</em> panel. Click the button <em>Choose</em> to set your preferred language for displaying websites. In the same dialog you can also add, remove and reorder languages.</li>
        <li><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong><br>
  Go into <em>Preferences</em> -&gt; <em>Settings</em> -&gt; <em>Network</em>. <em>Accept language</em> probably shows a * which is the default. If you click on the <em>Locale</em> button, you should be able to add your preferred language. If not, you can enter it manually.</li>
        <li><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong><br>
  <em>Edit</em> -&gt; <em>Preferences</em> -&gt; <em>Browser</em> -&gt; <em>Fonts, Languages</em></li>
        <li><strong><toc-add-entry name="iceweasel">IceCat (Iceweasel)</toc-add-entry></strong><br>
  <em>Edit</em> -&gt; <em>Preferences</em> -&gt; <em>Content</em> -&gt; <em>Languages</em> -&gt; <em>Choose</em></li>
        <li><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong><br>
  Click the <em>Tools</em> icon, select <em>Internet options</em>, switch to the <em>General</em> tab, and click the <em>Languages</em> button. Click <em>Set Language Preferences</em>, and in the following dialog you can add, remove and re-order languages.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong><br>
        Edit the file <em>~/.kde/share/config/kio_httprc</em> and include the following new line:<br>
        <code>Languages=fr;q=1.0, en;q=0.5</code></li>
        <li><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong><br>
        Edit the file <em>~/.lynxrc</em> and enter the following line:<br>
        <code>preferred_language=fr; q=1.0, en; q=0.5</code><br>
        Alternatively, you can open the browser's settings by pressing [O]. Scroll down to <em>Preferred language</em> and add the above.</li>
        <li><strong><toc-add-entry name="edge">Microsoft Edge</toc-add-entry></strong><br>
        <em>Settings and more</em>  -&gt; <em>Settings</em> -&gt; <em>Languages</em> -&gt; <em>Add languages</em><br>
        Click the three-dotted button next to a language entry for more options and to change the order.</li>
        <li><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong><br>
        <em>Settings</em> -&gt; <em>Browser</em> -&gt; <em>Languages</em> -&gt; <em>Preferred languages</em></li>
        <li><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong><br>
        Safari uses the system-wide settings on macOS and iOS, so in order to define your preferred language, please open <em>System Preferences</em> (macOS) or <em>Settings</em> (iOS).</li>
        <li><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong><br>
        Press [O] to open the <em>Option Setting Panel</em>, scroll down to <em>Network Settings</em> -&gt; <em>Accept-Language header</em>. Press [Enter] to change the settings (for example <code>fr; q=1.0, en; q=0.5</code>) and confirm with [Enter]. Scroll all the way down to [OK] to save your settings.</li>
        <li><strong><toc-add-entry name="vivaldi">Vivaldi</toc-add-entry></strong><br>
        Go to <em>Settings</em> -&gt; <em>General</em> -&gt; <em>Language</em> -&gt; <em>Accepted Languages</em>, click <em>Add Language</em> and choose one from the menu. Use the arrows to change the order of your preferences.</li>
      </ul>
    </div>
  </div>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 
브라우저 구성에서 원하는 언어를 선택하는 것이 더 좋지만, 쿠키로 설정을 무시하는 옵션이 있습니다.</p>
</aside>

<h2><a id="override">어떻게 설정을 무시하나</a></h2>

<p>
어떤 이유로든 브라우저 설정, 장치 또는 컴퓨팅 환경에서 기본 언어를 정의할 수 없는 경우 
최후의 수단으로 쿠키를 사용하여 기본 설정을 무시할 수 있습니다. 
목록 맨 위에 단일 언어를 추가하려면 아래 버튼 중 하나를 클릭하십시오.
</p>

<p><a
href="https://en.wikipedia.org/wiki/HTTP_cookie">쿠키</a>가 설정됩니다. 
한 달 동안 이 웹사이트를 방문하지 않으면 브라우저에서 자동으로 삭제합니다. 
물론 웹 브라우저에서 수동으로 쿠키를 삭제하거나 <em>브라우저 기본값</em> 버튼을 클릭하여 언제든지 쿠키를 삭제할 수 있습니다.
</p>

<protect pass=2>
<: print language_selector_buttons(); :>
</protect>


<h2><a id="fix">문제 해결</a></h2>

<p>
기본 언어를 설정하기 위한 모든 노력에도 불구하고 데비안 웹사이트가 잘못된 언어로 표시되는 경우가 있습니다. 
첫 번째 제안은 웹사이트를 다시 로드하기 전에 브라우저에서 로컬 캐시(디스크와 메모리 모두)를 정리하는 것입니다. 
<a href="#howtoset">브라우저 설정</a>이 확실하면 캐시가 손상되거나 잘못 구성된 것이 문제일 수 있습니다. 
점점 더 많은 ISP가 캐싱을 순 트래픽을 줄이는 방법으로 생각하기 때문에 이것은 요즘 심각한 문제가 되고 있습니다. 
프록시 서버를 사용하지 않는다고 생각하더라도 프록시 서버에 대한 <a href="#cache">섹션</a>을 읽으십시오.
</p>

<p>
항상 <a href="https://www.debian.org/">www.debian.org</a>에 문제가 있을 가능성이 있습니다. 
지난 몇 년 동안 보고된 소수의 언어 문제만이 우리 측의 버그로 인해 발생했지만 완전히 가능합니다. 
따라서 당사에 <a href="../contact">연락</a>하기 전에 먼저 자신의 설정과 잠재적인 캐싱 문제를 조사하는 것이 좋습니다. <a
href="https://www.debian.org/">https://www.debian.org/</a>가 동작하지만 
<a href="https://www.debian.org/mirror/list">미러</a> 중 하나가 동작하지 않으면 
이를 보고하여 미러 관리자에게 연락할 수 있습니다.
</p>

<h3><a name="cache">프록시 서버가 가진 잠재적 문제</a></h3>

<p>
프록시 서버는 본질적으로 자체 콘텐츠가 없는 웹 서버입니다. 
그들은 사용자와 실제 웹 서버 사이의 중간에 앉아 웹 페이지에 대한 요청을 잡고 페이지를 가져옵니다. 
그런 다음 콘텐츠를 사용자의 웹 브라우저로 전달하지만 나중에 요청하는 데 사용되는 캐시된 로컬 복사본도 만듭니다. 
이것은 많은 사용자가 동일한 페이지를 요청할 때 네트워크 트래픽을 실제로 줄일 수 있습니다.
</p>

<p>
이것은 대부분의 경우 좋은 생각이 될 수 있지만 캐시에 버그가 있는 경우에도 오류가 발생합니다. 
특히 일부 구형 프록시 서버는 콘텐츠 협상을 이해하지 못합니다. 
그 결과 나중에 다른 언어가 요청되더라도 페이지를 한 언어로 캐싱하고 이를 제공합니다. 
유일한 해결책은 캐싱 소프트웨어를 업그레이드하거나 교체하는 것입니다.
</p>

<p>
역사적으로 프록시 서버는 사람들이 그에 따라 웹 브라우저를 구성할 때만 사용되었습니다. 
그러나 더 이상 그렇지 않습니다. 
ISP가 투명 프록시를 통해 모든 HTTP 요청을 리디렉션할 수 있습니다. 
프록시가 콘텐츠 협상을 제대로 처리하지 않으면 사용자가 잘못된 언어로 캐시된 페이지를 받을 수 있습니다. 
이 문제를 해결할 수 있는 유일한 방법은 ISP에 불만을 제기하여 소프트웨어를 업그레이드하거나 교체하도록 하는 것입니다.
</p>
