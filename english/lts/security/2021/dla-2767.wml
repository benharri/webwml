<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Apache Santuario, XML Security for Java, is vulnerable to an issue where the
<q>secureValidation</q> property is not passed correctly when creating a KeyInfo
from a KeyInfoReference element. This allows an attacker to abuse an XPath
Transform to extract any local .xml files in a RetrievalMethod element.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.5.8-2+deb9u1.</p>

<p>We recommend that you upgrade your libxml-security-java packages.</p>

<p>For the detailed security status of libxml-security-java please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxml-security-java">https://security-tracker.debian.org/tracker/libxml-security-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2767.data"
# $Id: $
