# Templates files for webwml modules
# Copyright (C) 2003, 2008, 2011 Software in the Public Interest, Inc.
#
# Pierre Machard <pmachard@debian.org>, 2003, 2004.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2008.
# David Prévot <david@tilapin.org>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: debian webwml templates 0.1\n"
"PO-Revision-Date: 2011-04-22 22:12-0400\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../english/template/debian/wnpp.wml:8
msgid "No requests for adoption"
msgstr "Pas de demande d'adoption"

#: ../../english/template/debian/wnpp.wml:12
msgid "No orphaned packages"
msgstr "Aucun paquet abandonné"

#: ../../english/template/debian/wnpp.wml:16
msgid "No packages waiting to be adopted"
msgstr "Aucun paquet en attente d'adoption"

#: ../../english/template/debian/wnpp.wml:20
msgid "No packages waiting to be packaged"
msgstr "Aucun paquet en attente d'être empaqueté"

#: ../../english/template/debian/wnpp.wml:24
msgid "No Requested packages"
msgstr "Pas de paquet demandé"

#: ../../english/template/debian/wnpp.wml:28
msgid "No help requested"
msgstr "Pas d'aide demandée"

#. define messages for timespans
#. first the ones for being_adopted (ITAs)
#: ../../english/template/debian/wnpp.wml:34
msgid "in adoption since today."
msgstr "en cours d'adoption depuis aujourd'hui."

#: ../../english/template/debian/wnpp.wml:38
msgid "in adoption since yesterday."
msgstr "en cours d'adoption depuis hier."

#: ../../english/template/debian/wnpp.wml:42
msgid "%s days in adoption."
msgstr "en cours d'adoption depuis %s jours."

#: ../../english/template/debian/wnpp.wml:46
msgid "%s days in adoption, last activity today."
msgstr ""
"en cours d'adoption depuis %s jours, dernière modification aujourd'hui."

#: ../../english/template/debian/wnpp.wml:50
msgid "%s days in adoption, last activity yesterday."
msgstr "en cours d'adoption depuis %s jours, dernière modification hier."

#: ../../english/template/debian/wnpp.wml:54
msgid "%s days in adoption, last activity %s days ago."
msgstr ""
"en cours d'adoption depuis %s jours, dernière modification il y a %s jours."

#. timespans for being_packaged (ITPs)
#: ../../english/template/debian/wnpp.wml:60
msgid "in preparation since today."
msgstr "en préparation depuis aujourd'hui."

#: ../../english/template/debian/wnpp.wml:64
msgid "in preparation since yesterday."
msgstr "en préparation depuis hier."

#: ../../english/template/debian/wnpp.wml:68
msgid "%s days in preparation."
msgstr "en préparation depuis %s jours."

#: ../../english/template/debian/wnpp.wml:72
msgid "%s days in preparation, last activity today."
msgstr "en préparation depuis %s jours, dernière modification aujourd'hui."

#: ../../english/template/debian/wnpp.wml:76
msgid "%s days in preparation, last activity yesterday."
msgstr "en préparation depuis %s jours, dernière modification hier."

#: ../../english/template/debian/wnpp.wml:80
msgid "%s days in preparation, last activity %s days ago."
msgstr "en préparation depuis %s jours, dernière modification il y a %s jours."

#. timespans for request for adoption (RFAs)
#: ../../english/template/debian/wnpp.wml:85
msgid "adoption requested since today."
msgstr "adoption demandée depuis aujourd'hui."

#: ../../english/template/debian/wnpp.wml:89
msgid "adoption requested since yesterday."
msgstr "adoption demandée depuis hier."

#: ../../english/template/debian/wnpp.wml:93
msgid "adoption requested since %s days."
msgstr "adoption demandée depuis %s jours."

#. timespans for orphaned packages (Os)
#: ../../english/template/debian/wnpp.wml:98
msgid "orphaned since today."
msgstr "abandonné depuis aujourd'hui."

#: ../../english/template/debian/wnpp.wml:102
msgid "orphaned since yesterday."
msgstr "abandonné depuis hier."

#: ../../english/template/debian/wnpp.wml:106
msgid "orphaned since %s days."
msgstr "abandonné depuis %s jours."

#. time spans for requested (RFPs) and help requested (RFHs)
#: ../../english/template/debian/wnpp.wml:111
msgid "requested today."
msgstr "demandé aujourd'hui."

#: ../../english/template/debian/wnpp.wml:115
msgid "requested yesterday."
msgstr "demandé hier."

#: ../../english/template/debian/wnpp.wml:119
msgid "requested %s days ago."
msgstr "demandé il y a %s jours."

#: ../../english/template/debian/wnpp.wml:133
msgid "package info"
msgstr "renseignements sur le paquet"

#. popcon rank for RFH bugs
#: ../../english/template/debian/wnpp.wml:139
msgid "rank:"
msgstr "rang :"
