<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue has been discovered in gmp: GNU Multiple Precision Arithmetic Library.
It was discovered that integer overflow is possible in mpz/inp_raw.c and
resultant buffer overflow via crafted input, leading to a segmentation fault
on 32-bit platforms.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2:6.1.2+dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your gmp packages.</p>

<p>For the detailed security status of gmp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gmp">https://security-tracker.debian.org/tracker/gmp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2837.data"
# $Id: $
