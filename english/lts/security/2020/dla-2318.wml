<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The following CVE(s) have been reported against src:wpa.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10064">CVE-2019-10064</a>

    <p>hostapd before 2.6, in EAP mode, makes calls to the rand()
    and random() standard library functions without any preceding
    srand() or srandom() call, which results in inappropriate
    use of deterministic values. This was fixed in conjunction
    with <a href="https://security-tracker.debian.org/tracker/CVE-2016-10743">CVE-2016-10743</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12695">CVE-2020-12695</a>

    <p>The Open Connectivity Foundation UPnP specification before
    2020-04-17 does not forbid the acceptance of a subscription
    request with a delivery URL on a different network segment
    than the fully qualified event-subscription URL, aka the
    CallStranger issue.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2:2.4-1+deb9u7.</p>

<p>We recommend that you upgrade your wpa packages.</p>

<p>For the detailed security status of wpa please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wpa">https://security-tracker.debian.org/tracker/wpa</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2318.data"
# $Id: $
