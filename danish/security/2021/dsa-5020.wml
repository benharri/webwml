#use wml::debian::translation-check translation="9d48707cc1410dc9bc5673e6b67304a923f19557" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Chen Zhaojun fra Alibaba Cloud Security Team opdagede en kritisk 
sikkerhedssårbarhed i Apache Log4j, et populært logningsframework til Java. 
JNDI-funktioner anvendt i opsætningen, logmeddelelser og parametre beskytter 
ikke mod angriberkontrolleret LDAP og andre JNDI-relaterede endpoints.  En 
angriber, der kan kontrollere logmeddelelser eller logmeddelelsesparametre, 
kunne udføre vilkårlig kode indlæst fra LDAP-serverne, når erstatning af 
meddelelsesopslag er aktiveret.  Fra version 2.15.0 er denne virkemåde som 
standard deaktiveret.</p>

<p>Denne opdatering retter også 
<a href="https://security-tracker.debian.org/tracker/CVE-2020-9488">\
CVE-2020-9488</a> i den gamle stabile distribution (buster).  Ukorrekt 
validering af certifikater med ikke overensstemmende vært i Apache Log4j 
SMTP-appenderen.  Det kunne være muligt at opsnappe en SMTPS-forbindelse af et 
manden i midten-angreb, hvilket kunne lække enhver logmeddelelse, der er sendt 
gennem denne appender.</p>

<p>I den gamle stabile distribution (buster), er dette problem rettet
i version 2.15.0-1~deb10u1.</p>

<p>I den stabile distribution (bullseye), er dette problem rettet i
version 2.15.0-1~deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine apache-log4j2-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende apache-log4j2, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5020.data"
