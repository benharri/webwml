#use wml::debian::translation-check translation="1721318e4931a6644ae5d066132365cb278ba026" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans puma, un
serveur HTTP hautement concurrent pour les applications Ruby/Rack.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11076">CVE-2020-11076</a>

<p>En utilisant un en-tête d’encodage de transfert non valable, un attaquant
pourrait introduire subrepticement une réponse HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11077">CVE-2020-11077</a>

<p>Un client pourrait dissimuler une requête à travers un mandataire, amenant le
mandataire à renvoyer une réponse à un client inconnu. Si le mandataire utilise
des connexions persistantes et que le client ajoute une autre requête à l’aide
d’enchainements (<q>pipelining</q>) HTTP, un mandataire pourrait la confondre
comme étant le corps de la première requête. Puma, cependant, le verrait comme
deux requêtes, et lors du traitement de la seconde requête, renverrait une
réponse inattendue par le mandataire. Si le mandataire avait réutilisé la
connexion persistante vers Puma pour envoyer une autre requête à un client
différent, la seconde réponse du premier client serait envoyée au second
client.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 3.6.0-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets puma.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de puma, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/puma">https://security-tracker.debian.org/tracker/puma</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2398.data"
# $Id: $
