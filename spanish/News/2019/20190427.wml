#use wml::debian::translation-check translation="f797e8b952b201d953c4e7de221fd635808136e4"
<define-tag pagetitle>Debian 9 actualizado: publicada la versión 9.9</define-tag>
<define-tag release_date>2019-04-27</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la novena actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<p>Como una cuestión particular de esta versión en concreto, quienes utilicen <q>apt-get</q>
para la actualización tendrán que asegurarse de utilizar la orden
<q>dist-upgrade</q> para incluir la actualización a los últimos paquetes del núcleo. Quienes utilicen otras herramientas
como <q>apt</q> o <q>aptitude</q> deberían utilizar la orden <q>upgrade</q>.</p>

<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction audiofile "Corrige problemas de denegación de servicio [CVE-2018-13440] y de desbordamiento de memoria [CVE-2018-17095]">
<correction base-files "Actualizado para esta versión">
<correction bwa "Corrige desbordamiento de memoria [CVE-2019-10269]">
<correction ca-certificates-java "Corrige códigos específicos para bash en postinst y en jks-keystore">
<correction cernlib "Aplica la opción de optimización -O a los módulos Fortran en lugar de -O2, que genera código inválido; corrige fallo de compilación en arm64 inhabilitando PIE para los ejecutables Fortran">
<correction choose-mirror "Actualiza la lista de réplicas incluida">
<correction chrony "Corrige el registro de medidas y estadísticas y la parada de chronyd en algunas plataformas cuando está habilitado el filtrado con seccomp">
<correction ckermit "Elimina la comprobación de la versión de OpenSSL">
<correction clamav "Corrige acceso a la memoria dinámica («heap») fuera de límites al examinar documentos PDF [CVE-2019-1787], ficheros PE empaquetados con Aspack [CVE-2019-1789] o ficheros OLE2 [CVE-2019-1788]">
<correction dansguardian "Añade <q>missingok</q> a la configuración de logrotate">
<correction debian-installer "Recompilado contra proposed-updates">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction debian-security-support "Actualiza estados de soporte">
<correction diffoscope "Corrige pruebas para que funcionen con Ghostscript 9.26">
<correction dns-root-data "Actualiza datos raíz («root data») a 2019031302">
<correction dnsruby "Añade nueva clave raíz (KSK-2017); ruby 2.3.0 desaconseja usar TimeoutError, usa en su lugar Timeout::Error">
<correction dpdk "Nueva versión «estable» del proyecto original">
<correction edk2 "Corrige desbordamiento de memoria en servicio BlockIo [CVE-2018-12180]; DNS: comprueba tamaño del paquete recibido antes de usarlo [CVE-2018-12178]; corrige desbordamiento de pila con BMP corrupto [CVE-2018-12181]">
<correction firmware-nonfree "atheros / iwlwifi: actualiza firmware de BlueTooth [CVE-2018-5383]">
<correction flatpak "Rechaza todos los ioctls que el núcleo interpretaría como TIOCSTI [CVE-2019-10063]">
<correction geant321 "Recompilado contra cernlib con optimizaciones de Fortran fijas">
<correction gnome-chemistry-utils "Deja de compilar el paquete obsoleto gcu-plugin">
<correction gocode "gocode-auto-complete-el: pasa auto-complete-el a Pre-Depends para asegurar actualizaciones satisfactorias">
<correction gpac "Corrige desbordamientos de memoria [CVE-2018-7752 CVE-2018-20762], desbordamientos de memoria dinámica («heap») [CVE-2018-13005 CVE-2018-13006 CVE-2018-20761] y escrituras fuera de límites [CVE-2018-20760 CVE-2018-20763]">
<correction icedtea-web "Deja de compilar la extensión («plugin») de navegador, ya no funciona con Firefox 60">
<correction igraph "Corrige una caída al cargar ficheros GraphML mal construidos [CVE-2018-20349]">
<correction jabref "Corrige ataque de entidad externa XML [CVE-2018-1000652]">
<correction java-common "Elimina el paquete default-java-plugin, ya que la extensión («plugin») Xul de icedtea-web se va a eliminar">
<correction jquery "Evita contaminación de Object.prototype [CVE-2019-11358]">
<correction kauth "Corrige gestión insegura de parámetros en utilidades de ayuda («helpers») [CVE-2019-7443]">
<correction libdate-holidays-de-perl "Añade el 8 de marzo (de 2019 en adelante) y el 8 de mayo (solo en 2020) como festivos (solo en Berlin)">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libreoffice "Introduce la próxima era japonesa nengō 'Reiwa'; hace que -core entre en conflicto con openjdk-8-jre-headless (= 8u181-b13-2~deb9u1), que tiene una ClassPathURLCheck rota">
<correction linux "Nueva versión «estable» del proyecto original">
<correction linux-latest "Actualizado para la ABI -9 del núcleo">
<correction mariadb-10.1 "Nueva versión «estable» del proyecto original">
<correction mclibs "Recompilado contra cernlib con optimizaciones de Fortran fijas">
<correction ncmpc "Corrige desreferencia de puntero NULL [CVE-2018-9240]">
<correction node-superagent "Corrige ataques mediante bombas ZIP [CVE-2017-16129]; corrige error de sintaxis">
<correction nvidia-graphics-drivers "Nueva versión «estable» del proyecto original [CVE-2018-6260]">
<correction nvidia-settings "Nueva versión «estable» del proyecto original">
<correction obs-build "No permite la escritura de ficheros en el sistema anfitrión [CVE-2017-14804]">
<correction paw "Recompilado contra cernlib con optimizaciones Fortran fijas">
<correction perlbrew "Permite las URL HTTPS de CPAN">
<correction postfix "Nueva versión «estable» del proyecto original">
<correction postgresql-9.6 "Nueva versión «estable» del proyecto original">
<correction psk31lx "Ordena los números de versión correctamente para evitar problemas potenciales en las actualizaciones">
<correction publicsuffix "Actualiza los datos incluidos">
<correction pyca "Añade <q>missingok</q> a la configuración de logrotate">
<correction python-certbot "Vuelve a debhelper compat 9 para asegurar que los temporizadores de systemd arrancan correctamente">
<correction python-cryptography "Elimina BIO_callback_ctrl: el prototipo difiere de la definición de él dada por OpenSSL tras su modificación (corrección) en OpenSSL">
<correction python-django-casclient "Aplica la corrección del middleware django 1.10; python(3)-django-casclient: corrige dependencias con python(3)-django, que faltaban">
<correction python-mode "Elimina soporte para xemacs21">
<correction python-pip "Captura correctamente los HTTPError de solicitudes en index.py">
<correction python-pykmip "Corrige problema de denegación de servicio potencial [CVE-2018-1000872]">
<correction r-cran-igraph "Corrige denegación de servicio mediante objeto modificado [CVE-2018-20349]">
<correction rails "Corrige problemas de revelación de información [CVE-2018-16476 CVE-2019-5418] y problema de denegación de servicio [CVE-2019-5419]">
<correction rsync "Varias correcciones de seguridad para zlib [CVE-2016-9840 CVE-2016-9841 CVE-2016-9842 CVE-2016-9843]">
<correction ruby-i18n "Evita una vulnerabilidad de denegación de servicio remota [CVE-2014-10077]">
<correction ruby2.3 "Corrige FTBFS">
<correction runc "Corrige vulnerabilidad de elevación a privilegios de root [CVE-2019-5736]">
<correction systemd "journald: corrige fallo de aserción sobre journal_file_link_data; tmpfiles: corrige <q>e</q> para soportar patrones de nombres de fichero estilo intérprete de órdenes («shell»); mount-util: acepta que name_to_handle_at() podría fallar con EPERM; automount: acusa recibo de peticiones de automount también cuando ya están montadas [CVE-2018-1049]; corrige elevación potencial a privilegios de root [CVE-2018-15686]">
<correction twitter-bootstrap3 "Corrige problema de cross site scripting en descripciones emergentes («tooltips») o en «popovers» [CVE-2019-8331]">
<correction tzdata "Nueva versión del proyecto original">
<correction unzip "Corrige desbordamiento de memoria en archivos ZIP protegidos con contraseña [CVE-2018-1000035]">
<correction vcftools "Corrige revelación de información [CVE-2018-11099] y denegación de servicio [CVE-2018-11129 CVE-2018-11130] por medio de ficheros manipulados">
<correction vips "Corrige desreferencia de puntero de función NULL [CVE-2018-7998] y acceso a memoria no inicializada [CVE-2019-6976]">
<correction waagent "Nueva versión del proyecto original, con muchas correcciones de Azure [CVE-2019-0804]">
<correction yorick-av "Reescala las marcas de tiempo de fotograma; establece el tamaño de la zona de memoria VBV para ficheros MPEG1/2">
<correction zziplib "Corrige acceso a memoria inválida [CVE-2018-6381], error de bus [CVE-2018-6540], lectura fuera de límites [CVE-2018-7725], caída por fichero zip manipulado [CVE-2018-7726] y fuga de contenido de la memoria [CVE-2018-16548]; rechaza el fichero ZIP si el tamaño del directorio central y/o el desplazamiento al comienzo del directorio central apuntan más allá del final del fichero ZIP [CVE-2018-6484, CVE-2018-6541, CVE-2018-6869]">
</table>

<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2018 4259 ruby2.3>
<dsa 2018 4332 ruby2.3>
<dsa 2018 4341 mariadb-10.1>
<dsa 2019 4373 coturn>
<dsa 2019 4374 qtbase-opensource-src>
<dsa 2019 4377 rssh>
<dsa 2019 4385 dovecot>
<dsa 2019 4387 openssh>
<dsa 2019 4388 mosquitto>
<dsa 2019 4389 libu2f-host>
<dsa 2019 4390 flatpak>
<dsa 2019 4391 firefox-esr>
<dsa 2019 4392 thunderbird>
<dsa 2019 4393 systemd>
<dsa 2019 4394 rdesktop>
<dsa 2019 4396 ansible>
<dsa 2019 4397 ldb>
<dsa 2019 4398 php7.0>
<dsa 2019 4399 ikiwiki>
<dsa 2019 4400 openssl1.0>
<dsa 2019 4401 wordpress>
<dsa 2019 4402 mumble>
<dsa 2019 4403 php7.0>
<dsa 2019 4405 openjpeg2>
<dsa 2019 4406 waagent>
<dsa 2019 4407 xmltooling>
<dsa 2019 4408 liblivemedia>
<dsa 2019 4409 neutron>
<dsa 2019 4410 openjdk-8>
<dsa 2019 4411 firefox-esr>
<dsa 2019 4412 drupal7>
<dsa 2019 4413 ntfs-3g>
<dsa 2019 4414 libapache2-mod-auth-mellon>
<dsa 2019 4415 passenger>
<dsa 2019 4416 wireshark>
<dsa 2019 4417 firefox-esr>
<dsa 2019 4418 dovecot>
<dsa 2019 4419 twig>
<dsa 2019 4420 thunderbird>
<dsa 2019 4422 apache2>
<dsa 2019 4423 putty>
<dsa 2019 4424 pdns>
<dsa 2019 4425 wget>
<dsa 2019 4426 tryton-server>
<dsa 2019 4427 samba>
<dsa 2019 4428 systemd>
<dsa 2019 4429 spip>
<dsa 2019 4430 wpa>
<dsa 2019 4431 libssh2>
<dsa 2019 4432 ghostscript>
<dsa 2019 4433 ruby2.3>
<dsa 2019 4434 drupal7>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction gcontactsync "Incompatible con versiones más recientes de firefox-esr">
<correction google-tasks-sync "Incompatible con versiones más recientes de firefox-esr">
<correction mozilla-gnome-kerying "Incompatible con versiones más recientes de firefox-esr">
<correction tbdialout "Incompatible con versiones más recientes de thunderbird">
<correction timeline "Incompatible con versiones más recientes de thunderbird">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas por
esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>

