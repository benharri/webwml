<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jacek Konieczny discovered a SQL injection vulnerability in the back-sql
backend to slapd in OpenLDAP, a free implementation of the Lightweight
Directory Access Protocol, allowing an attacker to alter the database
during an LDAP search operations when a specially crafted search filter
is processed.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.4.44+dfsg-5+deb9u9.</p>

<p>We recommend that you upgrade your openldap packages.</p>

<p>For the detailed security status of openldap please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openldap">https://security-tracker.debian.org/tracker/openldap</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3017.data"
# $Id: $
