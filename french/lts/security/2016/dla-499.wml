#use wml::debian::translation-check translation="7e6b86e512d1e203a20619cdee7230be27c2eae5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8865">CVE-2015-8865</a>
<p>La fonction file_check_mem dans funcs.c dans la version de file
antérieure à 5.23, telle qu'utilisée dans le composant Fileinfo dans les
versions de PHP antérieures à 5.5.34, 5.6.x antérieures à 5.6.20, et 7.x
antérieures à 7.0.5, gère mal les sauts de niveau de continuation. Cela
permet à des attaquants en fonction du contexte de provoquer un déni de
service (dépassement de tampon et plantage de l'application) ou
éventuellement d'exécuter du code arbitraire à l'aide d'un fichier magique
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8866">CVE-2015-8866</a>
<p>La configuration de libxml_disable_entity_loader est partagée entre les
processus légers ext/libxml/libxml.c dans les versions de PHP antérieures
à 5.5.22 et 5.6.x antérieures à 5.6.6, quand PHP-FPM est utilisé, et
n'isole pas chaque processus des modifications de
libxml_disable_entity_loader dans d'autres processus. Cela permet
à des attaquants distants de conduire des attaques d'entité externe XML (XXE)
et d'expansion d'entité (XEE) à l'aide d'un document contrefait XML, un
problème lié au <a href="https://security-tracker.debian.org/tracker/CVE-2015-5161">CVE-2015-5161</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8878">CVE-2015-8878</a>
<p>main/php_open_temporary_file.c dans les versions de PHP antérieures
à 5.5.28 et 5.6.x antérieures à 5.6.12 n'assure pas la sécurité des
processus légers. Cela permet à des attaquants distants de provoquer un
déni de service (situation de compétition et corruption de mémoire de tas)
en exploitant une application qui réalise de multiples accès aux fichiers
temporaires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8879">CVE-2015-8879</a>
<p>La fonction odbc_bindcols dans ext/odbc/php_odbc.c dans les versions de
PHP antérieures à 5.6.12 gère mal le comportement du pilote pour les
colonnes SQL_WVARCHAR. Cela permet à des attaquants distants de provoquer
un déni de service (plantage de l'application) dans des circonstances
opportunistes en exploitant l'utilisation de la fonction odbc_fetch_array
pour accéder à un certain type de table de Microsoft SQL Server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4070">CVE-2016-4070</a>
<p>Un dépassement d'entier dans la fonction php_raw_url_encode dans
ext/standard/url.c dans les versions de PHP antérieures à 5.5.34, 5.6.x
antérieures à 5.6.20, et 7.x antérieures à 7.0.5 permet à des attaquants
distants de provoquer un déni de service (plantage de l'application)
à l'aide d'une longue chaîne envoyée à la fonction rawurlencode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4071">CVE-2016-4071</a>
<p>Une vulnérabilité de chaîne de format dans la fonction php_snmp_error
dans ext/snmp/snmp.c dans les versions de PHP antérieures à 5.5.34, 5.6.x
antérieures à 5.6.20, et 7.x antérieures à 7.0.5 permet à des attaquants
distants d'exécuter du code arbitraire à l'aide de prescripteurs de chaîne
de formatage dans un appel SNMP::get.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4072">CVE-2016-4072</a>
<p>L'extension Phar dans les versions de PHP antérieures à 5.5.34, 5.6.x
antérieures à 5.6.20, et 7.x antérieures à 7.0.5 permet à des attaquants
distants d'exécuter du code arbitraire à l'aide d'un nom de fichier
contrefait, comme démontré par le mauvais traitement des caractères \0 par
la fonction phar_analyze_path dans ext/phar/phar.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4073">CVE-2016-4073</a>
<p>Plusieurs dépassements d'entier dans la fonction mbfl_strcut dans
ext/mbchaîne/libmbfl/mbfl/mbfilter.c dans les versions de PHP antérieures
à 5.5.34, 5.6.x antérieures à 5.6.20, et 7.x antérieures à 7.0.5 permettent
à des attaquants distants de provoquer un déni de service (plantage de
l'application) ou éventuellement d'exécuter du code arbitraire à l'aide
d'un appel mb_strcut contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4343">CVE-2016-4343</a>
<p>La fonction phar_make_dirstream dans ext/phar/dirstream.c dans les
versions de PHP antérieures à 5.6.18 et 7.x antérieures à 7.0.3 gère mal
les fichiers de taille zéro ././@LongLink. Cela permet à des attaquants
distants de provoquer un déni de service (déréférencement de pointeur non
initialisé) ou éventuellement d'avoir un autre impact non précisé à l'aide
d'une archive TAR contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4537">CVE-2016-4537</a>
<p>La fonction bcpowmod fonction dans ext/bcmath/bcmath.c dans les versions
de PHP antérieures à 5.5.35, 5.6.x antérieures à 5.6.21, et 7.x antérieures
à 7.0.6 accepte un entier négatif comme argument d'échelle. Cela permet
à des attaquants distants de provoquer un déni de service ou éventuellement
d'avoir un autre impact non précisé à l'aide d'un appel contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4539">CVE-2016-4539</a>
<p>La fonction xml_parse_into_struct dans ext/xml/xml.c dans les versions
de PHP antérieures à 5.5.35, 5.6.x antérieures à 5.6.21 et 7.x antérieures
à 7.0.6 permet à des attaquants distants de provoquer un déni de service
(lecture hors limites de tampon et erreur de segmentation) ou
éventuellement d'avoir un autre impact non précisé au moyen de données XML
contrefaites dans le second argument, menant à un niveau d'analyse de zéro.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4540">CVE-2016-4540</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4541">CVE-2016-4541</a>
<p>La fonction grapheme_strpos dans ext/intl/grapheme/grapheme_string.c
dans les versions antérieures à 5.5.35, 5.6.x antérieures à 5.6.21 et 7.x
antérieures à 7.0.6 permet à des attaquants distants de provoquer un déni
de service (lecture hors limites) ou éventuellement d'avoir un autre impact
non précisé au moyen d'offset négatif.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4542">CVE-2016-4542</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4543">CVE-2016-4543</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4544">CVE-2016-4544</a>
<p>La fonction exif_process_* dans ext/exif/exif.c dans les versions de PHP
antérieures à 5.5.35, 5.6.x antérieures à 5.6.21 et 7.x antérieures à 7.0.6
ne valide pas la taille des IFD. Cela permet à des attaquants distants de
provoquer un déni de service (lecture hors limites) ou éventuellement
d'avoir un autre impact non précisé au moyen de données d'en-tête
contrefaites.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans php5
version 5.4.45-0+deb7u3.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-499.data"
# $Id: $
