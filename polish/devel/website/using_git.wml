#use wml::debian::template title="Używanie gita do manipulowania kodem źródłowym witryny"
#use wml::debian::translation-check translation="775bde4698a5b024eb1e3ba05a53cf38a0eb1fd5"

<p>Git jest <a href="https://en.wikipedia.org/wiki/Version_control">systemem 
kontroli wersji</a>, który pomaga zarządzać pracą wielu osób pracujących 
jednocześnie nad tym samym materiałem. Każdy użytkownik może posiadać 
lokalną kopię głównego repozytorium. Kopia lokalna może być na tej samej 
maszynie albo w dowolnym miejscu na świecie. Użytkownicy mogą następnie 
modyfikować kopię lokalną według własnego uznania i kiedy modyfikowany 
materiał jest gotowy, zatwierdzają zmiany i wysyłają je z powrotem do 
głównego repozytorium.</p>

<p>Git nie pozwoli wysłać commita, jeśli repozytorium zdalne posiada nowsze 
commity (zmiany) niż kopia lokalna w tej samej gałęzi. W takim przypadku, gdy 
istnieje konflikt, należy najpierw pobrać i zaktualizować kopię lokalną a 
następnie w razie potrzeby wykonać <code>rebase</code> swoich nowych zmian 
bazując na ostatniej wersji.
</p>

<h3><a name="write-access">git - dostęp do zapisu</a></h3>

<p>
Osoby potrzebujące dostępu do zapisu w repozytorium muszą poprosić o to
poprzez stronę internetową <url https://salsa.debian.org/webmaster-team/webwml/> 
po zalogowaniu się na platformę Debian Salsa.
Osoby nowe w pracach nad stronami Debiana, nie mające wcześniejszych 
doświadczeń w tej dziedzinie, powinny wysłać także wiadomość do 
<a href="mailto:debian-www@lists.debian.org">debian-www@lists.debian.org</a>
i przedstawić się. Prosimy napisać coś użytecznego w swej prośbie: nad którym 
językiem lub częścią witryny planujesz pracować, i kto może za Ciebie poręczyć. 
</p>

<h3><a name="work-on-repository">Praca z repozytorium</a></h3>

<h4><a name="clone-local-repo-copy">Sklonuj lokalną kopię repozytorium</a></h4>

<p>Aby móc pracować z repozytorium, na początku należy zainstalować program git. 
Następnie należy skonfigurować użytkownika oraz adres mailowy na swoim 
komputerze (aby dowiedzieć się jak to zrobić, proszę odwołać się do ogólnej 
dokumentacji gita). Po wykonaniu tych czynności można sklonować repozytorium 
(innymi słowy, utworzyć lokalną kopię) używając jednego z dwóch sposobów.</p>

<p>Zalecanym sposobem pracy nad webwml, jest rozpoczęcie od zarejestrowania 
się na salsa.debian.org i włączenie dostępu git SSH poprzez przesłanie 
publicznego klucza SSH do swojego konta. Szczegółowe informacji jak to 
zrobić znajdują się na <a
href="https://salsa.debian.org/help/ssh/index.md">stronach pomocy salsa</a>.
Po wykonaniu tych czynności można sklonować repozytorium webwml używając 
następującego polecenia:</p>

<pre>
   git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>

<p>Jeżeli nie mamy konta salsa, alternatywną metodą jest sklonowanie 
repozytorium przy użyciu protokołu HTTPS:</p>

<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>

<p>Polecenie to utworzy taką samą lokalną kopię repozytorium, ale 
nie pozwoli bezpośrednio wysyłać zmian</p>

<p>Sklonowanie całego repozytorium webwml będzie wymagało pobrania 
około 500MB danych, a zatem może to być trudne dla osób z wolnym 
lub niestabilnym połączeniem internetowym. Aby zmniejszyć ilość 
danych przy pierwszym pobraniu, można spróbować płytkiego klonowania 
z minimalnym zagłębieniem:</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Po uzyskaniu użytecznego (płytkiego) repozytorium, można pogłębić 
lokalną płytką kopię i ostatecznie przekonwertować ją do pełnego lokalnego
repozytorium:</p>

<pre>
  git fetch --deepen=1000 # pogłębienie repozytorium o kolejne 1000 commitów
  git fetch --unshallow   # pobranie wszystkich brakujących commitów, konwersja repozytorium do stanu kompletnego
</pre>

<h4><a name="partial-content-checkout">Częściowe sprawdzanie zawartości</a></h4>

<p>W następujący sposób można utworzyć checkout tylko dla wybranego zestawu stron:</p>

<pre>
   $ git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git
   $ cd webwml
   $ git config core.sparseCheckout true
   W webwml: Utwórz plik .git/info/sparse-checkout z następującą zawartością
   (jeśli potrzebujesz podstawowych plików oraz tłumaczeń języka angielskiego, katalońskiego i hiszpańskiego):
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
   Następnie:
   $ git checkout --
</pre>

<h4><a name="submit-changes">Przesyłanie lokalnych zmian</a></h4>

<p>Co kilka dni (a zdecydowanie przed rozpoczęciem edycji) należy wykonać</p>

<pre>
   git pull
</pre>

<p>aby otrzymać zmienione pliki z repozytorium.</p>

<p>Uwaga: git jest rozproszonym (nie scentralizowanym) systemem kontroli 
wersji. Oznacza to, że kiedy robimy commit zmian, są one zachowywane tylko 
w naszym lokalnym repozytorium. Aby udostępnić je innym, należy 
wysłać nasze zmiany do centralnego repozytorium na salsa.</p>

<p><strong>Zatem edycja pliku w języku angielskim będzie przebiegać następująco:</strong></p>

<pre>
   $ git pull
</pre>

<p>Następnie wprowadzamy zmiany w plikach. Potem wysyłamy zmiany do lokalnego 
repozytorium używając polecenia:</p>

<pre>
   $ git add ścieżka/do/pliku(ów)
   $ git commit -m "Opis zmian"
</pre>

<p>Następnie wysyłamy zmiany do repozytorium Salsa:</p>

<pre>
   $ git push
</pre>

<p>To jest skrócony opis tego, jak używać gita do manipulowania kodem 
źródłowym stron Debiana. Aby uzyskać więcej informacji, proszę 
zapoznać się z dokumentacją gita.</p>

### FIXME: Is the following still true? holgerw
### FIXME: Seems not true anymore. Needs fixup. -- hosiet-guest
<h4><a name="closing-debian-bug-in-git-commits">Zamykanie błędów przy użyciu commitów</a></h4>

<p>
Jeżeli dołączymy <code>Closes: #</code><var>nnnnnn</var> w logu commita, 
wtedy błąd o numerze <code>#</code><var>nnnnnn</var> będzie zamknięty 
automatycznie po wysłaniu zmian. Dokładna forma tego wpisu jest taka sama
jak <a href="$(DOC)/debian-policy/ch-source.html#id24">w polityce Debiana</a>.</p>

<h4><a name="links-using-http-https">Linkowanie przy użyciu HTTP/HTTPS</a></h4>

<p>Wiele stron Debiana wspiera SSL, dlatego należy używać linków HTTPS 
gdzie jest to możliwe i sensowne. <strong>Jakkolwiek</strong> niektóre 
strony Debian/DebConf/SPI/itp albo nie wspierają HTTPS w ogóle, albo 
używają tylko SPI CA (i nie używają zaufanych przez wszystkie przeglądarki SSL CA).
Aby uniknąć powodowania komunikatów o błędach dla użytkowników spoza Debiana, 
nie należy stosować do takich stron linków z HTTPS.</p>

<p>Repozytorium git odrzuci commity zawierające czyste linki HTTP do 
stron Debiana, które wspierają HTTPS lub zawierające linki HTTPS 
do stron Debian/DebConf/SPI, o których wiemy że albo nie wspierają HTTPS
albo używają certyfikatów podpisanych tylko przez SPI.</p>

<h3><a name="translation-work">Praca nad tłumaczeniami</a></h3>

<p>Po zmianie tłumaczonego pliku, należy zaktualizować nagłówek 
translation-check, aby odpowiadał identyfikatorowi (hash) commita z 
odpowiednią zmianą w pliku angielskim. Identyfikator ten znajdziemy 
korzystając z polecenia</p>

<pre>
$ git log path/to/english/file
</pre>

<p>Jeżeli tworzymy nowe tłumaczenie pliku, możemy użyć skryptu 
<q>copypage.pl</q>, który utworzy szablon dla naszego języka, dołączając 
odpowiedni nagłówek tłumaczenia.</p>

<h3><a name="translation-smart-change">Tłumaczenie zmian przy użyciu skryptu smart_change.pl</a></h3>

<p><code>smart_change.pl</code> jest skryptem zaprojektowanym aby ułatwić 
wspólną aktualizację oryginalnego pliku oraz jego tłumaczeń. Istnieją dwa
sposoby użycia tego skryptu, w zależności od zmian jakie chcemy wprowadzić.</p>

<p>Aby użyć <code>smart_change</code> tylko do aktualizacji nagłówka 
translation-check, kiedy pracujemy nad plikiem ręcznie:</p>

<ol>
  <li>Wprowadzamy zmiany w oryginalnym pliku(ach) i robimy commit</li>
  <li>Aktualizujemy tłumaczenia</li>
  <li>Uruchamiamy smart_change.pl - zostaną pobrane zmiany i zaktualizowane 
    nagłówki w przetłumaczonych plikach</li>
  <li>Przeglądamy zmiany (np. przy użyciu "git diff")</li>
  <li>Robimy commit zmian w tłumaczeniach</li>
</ol>

<p>Albo, jeżeli używamy smart_change z wyrażeniami regularnymi do 
zrobienia wielu zmian w wielu plikach w jednym kroku:</p>

<ol>
  <li>Uruchamiamy <code>smart_change.pl -s s/FOO/BAR/ origfile1 origfile2 ...</code></li>
  <li>Przeglądamy zmiany (np. przy użyciu <code>git diff</code>)
  <li>Robimy commit oryginalnego pliku(ów)</li>
  <li>uruchamiamy <code>smart_change.pl origfile1 origfile2</code>
    (np. <strong>bez regexp</strong> tym razem); teraz będą zaktualizowane 
    nagłówki w przetłumaczonych plikach</li>
  <li>Na końcu, robimy commit zmian w tłumaczeniach</li>
</ol>

<p>Ten sposób jest bardziej skomplikowany niż poprzedni (wymagający dwóch 
commitów), ale nieunikniony ze względu na sposób, w jaki 
działają hashe commitów w git.</p>

<h3><a name="notifications">Otrzymywanie powiadomień</a></h3>

<p>Skonfigurowaliśmy projekt webwml na Salsa w taki sposób, aby commity 
pokazywały się na kanale IRC #debian-www.</p>

<p>Aby otrzymywać powiadomienia poprzez e-mail kiedy pojawią się commity 
w repozytorium, należy zapisać się do pseudopakietu <q>www.debian.org</q> 
poprzez tracker.debian.org i aktywować tam słowo kluczowe <q>vcs</q> 
wykonując następujące kroki (tylko raz):</p>

<ul>
  <li>Otworzyć przeglądarkę i przejść pod adres <url https://tracker.debian.org/pkg/www.debian.org></li>
  <li>Zapisać się do pseudopakietu <q>www.debian.org</q>. (Można uwierzytelnić się poprzez
      SSO lub zarejestrować email i hasło, jeśli jeszcze nie używamy 
      tracker.debian.org do innych celów).</li>
  <li>Przejść pod adres <url https://tracker.debian.org/accounts/subscriptions/>, następnie do <q>modify
      keywords</q>, zaznaczyć <q>vcs</q> (jeśli nie jest zaznaczony) i zapisać.</li>
  <li>Od tej chwili będziemy otrzymywać wiadomości kiedy ktoś zrobi commit do repozytorium webwml.
      Wkrótce dodamy inne repozytoria webmaster-team.</li>
</ul>
