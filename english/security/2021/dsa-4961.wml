<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Henry de Valence reported a flaw in the signature verification code in
Tor, a connection-based low-latency anonymous communication system. A
remote attacker can take advantage of this flaw to cause an assertion
failure, resulting in denial of service.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 0.3.5.16-1.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 0.4.5.10-1~deb11u1.</p>

<p>We recommend that you upgrade your tor packages.</p>

<p>For the detailed security status of tor please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/tor">https://security-tracker.debian.org/tracker/tor</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4961.data"
# $Id: $
