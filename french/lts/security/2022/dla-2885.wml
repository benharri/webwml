#use wml::debian::translation-check translation="d8bdac7cb5f11c0824d509e08a4fc7b3c4411db1" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs erreurs de lecture hors limites ont été découvertes dans
qtsvg-opensource-src. La plus grande menace d'après le
<a href="https://security-tracker.debian.org/tracker/CVE-2021-3481">CVE-2021-3481</a>
(au moins) porte sur la disponibilité de données confidentielles de
l'application.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 5.7.1~20161021-2.1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets
qtsvg-opensource-src.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de
qtsvg-opensource-src, veuillez consulter sa page de suivi de sécurité à
l'adresse :
<a href="https://security-tracker.debian.org/tracker/qtsvg-opensource-src">\
https://security-tracker.debian.org/tracker/qtsvg-opensource-src</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2885.data"
# $Id: $
