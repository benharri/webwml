#use wml::debian::translation-check translation="da347ceee9cca800740ef75deed5e600ef8e2b1d" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige sårbarheder er opdaget i openjpeg2, open source JPEG 
2000-codec'et, hvilke kunne udnyttes til at forårsage et lammelsesangreb eller 
muligvis fjernudførelse af kode.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17480">CVE-2017-17480</a>

    <p>Bufferoverløb i stakskrivning i codec'erne jp3d og jpwl, kunne medføre 
    lammelsesangreb eller fjernudførelse af kode gennem en fabrikeret jp3d- 
    eller jpwl-fil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5785">CVE-2018-5785</a>

    <p>Heltalsoverløb kunne medføre et lammelsesangreb gennem en fabrikeret 
    bmp-fil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6616">CVE-2018-6616</a>

    <p>Alt for mange iterationer kunne medføre et lammelsesangreb gennem en 
    fabrikeret bmp-fil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14423">CVE-2018-14423</a>

    <p>Division med nul-sårbarheder, kunne medføre et lammelsesangreb gennem en 
    fabrikeret j2k-fil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18088">CVE-2018-18088</a>

    <p>En nullpointerdereference kunne medføre lammelsesangreb gennem en 
    fabrikeret bmp-fil.</p></li>

</ul>

<p>I den stabile distribution (stretch), er disse problemer rettet i
version 2.1.2-1.1+deb9u3.</p>

<p>Vi anbefaler at du opgraderer dine openjpeg2-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende openjpeg2, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/openjpeg2">\
https://security-tracker.debian.org/tracker/openjpeg2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4405.data"
