<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The Qualys Research Labs discovered two vulnerabilities in util-linux's
libmount. These flaws allow an unprivileged user to unmount other users'
filesystems that are either world-writable themselves or mounted in a
world-writable directory 
(<a href="https://security-tracker.debian.org/tracker/CVE-2021-3996">\
CVE-2021-3996</a>), or to unmount FUSE filesystems that belong to certain other 
users 
(<a href="https://security-tracker.debian.org/tracker/CVE-2021-3995">\
CVE-2021-3995</a>).</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.36.1-8+deb11u1.</p>

<p>We recommend that you upgrade your util-linux packages.</p>

<p>For the detailed security status of util-linux please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/util-linux">\
https://security-tracker.debian.org/tracker/util-linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5055.data"
# $Id: $
